[![pipeline status](https://gitlab.com/carbans/carbans.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/carbans/carbans.gitlab.io/commits/master)

# carloslatorre.net

A personal site where you can find my blog and some other stuff.

It's hosted on GitLab Pages and the source code is available on GitLab, build with Hugo and GitLab CI.
