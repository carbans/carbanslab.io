---
title: Now
description: 'Page to talk about me and my present'
author: Carlos Latorre
---

This now page it's based on [Lazy Bear Page](https://lazybear.io/now/)

## What I do?

Building amazing infrastructure as a SRE, Platform Engineer, DevOps.

## Books
I'm currently reading those books:

- The Phoenix Project by Gene Kim
- El método Ikigai by Francesc Miralles and Hector García
- Hacking Kubernetes by Andrew Martin

## Watching

I rewatching Lost again with my partner.

## Projects

- Working hard into Nubulus Network
- [SimpleDNS](https://github.com/carbans/simpledns) => Help me understand DNS and it's implementation more deeply.
