---
title: "100 Days to Offload"
subtitle: ""
date: 2024-07-02T20:25:00+02:00
lastmd: 2024-07-02T20:25:00+02:00
draft: false
author: "Carlos Latorre"

tags: ['100DaysToOffload', 'writing', 'challenge']

toc: false
---

In the past months, I have enjoyed reading personal blogs and like small posts, I have decided to take the courage to start with a 100 Days To Offload Challenge.

The challenge pushes you to write about whatever you want and you don't worry about the form or the amount of words. I want to prove that I can write little posts and appreciate the value of this.

I will tag all the posts with #100DaysToOffload and I added a footer note for posts that show the progress of the challenge.


> Post 1/100 - Round 1 of the [100DaysToOffload](https://100daystooffload.com/) challenge!


