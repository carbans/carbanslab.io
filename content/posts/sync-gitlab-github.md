---
title: "Sync Repos Gitlab beetwen Github"
subtitle: ""
date: 2023-04-17T14:42:26+02:00
lastmod: 2023-04-17T14:42:26+02:00
draft: false
author: "Carlos Latorre"
authorLink: ""
description: ""

tags: ['git', 'gitlab', 'github', 'sync']
categories: ['git']

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: ""
featuredImagePreview: ""

toc:
  enable: true
math:
  enable: false
lightgallery: false
license: ""
---
I've been using Gitlab since January 17, 2016 and I'm very happy with this tool for years was my main repository of code. But now I have some of repositories in Github and I want to sync them with Gitlab, because I want that Gitlab to be a source of truth

I research a little bit and I found this [repo](https://github.com/wangchucheng/git-repo-sync) that show me how I can do it.
<!--more-->
First, we need to look at process of sync:

![Flow of sync](/images/github-sync-gitlab.png)

We need to start creating workflow file into your repo in Github, you can find the file in this [link](https://github.com/carbans/dotfiles/blob/master/.github/workflows/publishGitlab.yaml)

```yaml
name: GitlabSync

on:
  - push
  - delete

jobs:
  sync:
    runs-on: ubuntu-latest
    name: Git Repo Sync
    steps:
    - uses: actions/checkout@v3
      with:
        fetch-depth: 0
    - uses: wangchucheng/git-repo-sync@v0.1.0
      with:
        # Such as https://github.com/wangchucheng/git-repo-sync.git
        target-url: ${{ secrets.TARGET_URL }}
        # Such as wangchucheng
        target-username: ${{ secrets.TARGET_USERNAME }}
          # You can store token in your project's 'Setting > Secrets' and reference the name here. Such as ${{ secrets.ACCESS\_TOKEN }}
        target-token: ${{ secrets.TARGET_TOKEN }}
```

As you can see we need to create 3 secrets to use this workflow:

- TARGET_URL => https://gitlab.com/username/test.git
- TARGET_USERNAME => your username in Gitlab
- TARGET_TOKEN => Access token of your repo settings in Gitlab

Once you create the Token into Gitlab you need to add secrets into Github repository. To do this you need to go to Settings > Secrets and add the secrets.

These not it's the best way to do it, but it's a good way to start and simple way to do it and without any maintenance.
