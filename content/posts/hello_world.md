---
title: "Hello New World"
subtitle: ""
date: 2020-08-21T12:37:25+02:00
lastmod: 2020-08-21T12:37:25+02:00
draft: false
author: "Carlos Latorre"
authorLink: ""
description: ""

tags: ['hello', 'world', 'hugo']
categories: []

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: ""
featuredImagePreview: ""

toc:
  enable: false
math:
  enable: false
lightgallery: false
license: ""
---
Hello! This is my first post in Hugo. In the past, I had a blog in the same domain that used WordPress and it had some posts that I don't want to migrate here because it's a good opportunity to start a new blog.
<!--more-->
I choose Hugo because I don't want to maintain WordPress and I don't want to do an update every week. I don't feel comfortable right now with WordPress. I prefer the freedom of Hugo and I can practice the new cloud methodology that I learned in the last months.

In this new stage, I will try to write posts in English, (don't forget that my native language it's Spanish) , and I will try that the topic is going related to cloud and networking.

Thanks for reading me and we'll see you in the next post.


