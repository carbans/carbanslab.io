---
title: "System Design Interview Book"
subtitle: ""
date: 2023-04-17T11:12:58+02:00
lastmod: 2023-04-17T11:12:58+02:00
draft: true
author: "Carlos Latorre"
authorLink: ""
description: ""

tags: ['book', 'system design', 'interview', 'system design interview']
categories: ['books']

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: ""
featuredImagePreview: ""

toc:
  enable: true
math:
  enable: false
lightgallery: false
license: ""
---
Recently, I purchase the book System Design Interview by Alex Xu. I want to share with you my experience with this book and how you can use it to prepare for your next interview.
You can find this book in (Amazon)[https://www.amazon.com/System-Design-Interview-Insiders-Guide/dp/1736049119]
The structure of the book it's into chatper and each chapter has a topic of one of the most common system design interview questions. It's a good book to start with system design interview questions and it's a good book to have as a reference. Alex Xu creates a good structure to explain the topic and he gives you a lot of examples to understand the topic and extra resources to learn more about the topic. You can find into github (https://github.com/alex-xu-system/bytebytego/blob/main/system_design_links_vol2.md)[https://github.com/alex-xu-system/bytebytego/blob/main/system_design_links_vol2.md]

These is a list of some topics that you can find in this book
- 

<!--more-->
