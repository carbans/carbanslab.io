---
title: "Hugo Submodules"
subtitle: ""
date: 2020-09-27T18:35:34+02:00
lastmod: 2020-09-27T18:35:34+02:00
draft: false
author: ""
authorLink: ""
description: ""

tags: ['hugo', 'submodules', 'git']
categories: []

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: ""
featuredImagePreview: ""

toc:
  enable: false
math:
  enable: false
lightgallery: false
license: ""
---
Hello!! 

This blog it's made with Hugo, and today I'm going to give you some tips for sync content and git project across different machines.
<!--more-->
It's important know that when you pull your repo the submodules not pull with it. If you want to pull all include submodules you need to type this:
```bash
git clone --recursive url
```
If you have already cloned a repository and want to load it's submodules you need to type this:
```bash
git submodule update --init
```
Here I'm give you some links to explore and understading the submodules on git.

- https://opensource.com/article/20/5/git-submodules-subtrees
- https://git-scm.com/book/en/v2/Git-Tools-Submodules