---
title: "Resource Compilation 24/04/23"
subtitle: ""
date: 2023-04-24T16:13:11+02:00
lastmod: 2023-04-24T16:13:11+02:00
draft: true
author: "Carlos Latorre Sánchez"
authorLink: ""
description: ""

tags: ['resources', 'compilation', '2023', '04', '24']
categories: ['compilation']

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: ""
featuredImagePreview: ""

toc:
  enable: true
math:
  enable: false
lightgallery: false
license: ""
---
Today I want to share with you a compilation of resources that I have found interesting and that I think you will also like. I hope you enjoy it.
I plan made this compilation every week, it's difficult to find time to do it, but I will try to do it every week. I hope you like it.
<!--more-->
I structure this compilation in 2 sections:

- [Resources](#resources)
- [Tools](#tools)

## Resources

## Tools

I spend a lot of my time working with kubernetes and I use a lot of tools to help me with k8s. One of the magic of kubectl client is that you can extend it with plugins. I have a list of plugins that I use in my daily work. I want to share with you one plugins that I discover this week. It's **kubectl-oomd** and it's a plugin that helps you to find the pod that is causing the OOM (Out of Memory) in your cluster. It's a very useful plugin if you are working with kubernetes and you want to know which pod is causing the OOM in your cluster. You can find more information about this plugin in the following link:
[https://github.com/jdockerty/kubectl-oomd](https://github.com/jdockerty/kubectl-oomd)

You can test it with another project of this author called [oomer](https://github.com/jdockerty/oomer) that is a tool that helps you to simulate OOM with a little go program. In linux you can see if the exit code it's **137** it means that the process was killed by the OOM killer.



## Conclusions
