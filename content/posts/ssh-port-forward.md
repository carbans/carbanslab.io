---
title: "Using SSH to Port Forward"
subtitle: ""
date: 2023-02-27T15:34:18+01:00
lastmod: 2023-02-27T15:34:18+01:00
draft: true
author: ""
authorLink: ""
description: ""

tags: []
categories: []

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: ""
featuredImagePreview: ""

toc:
  enable: true
math:
  enable: false
lightgallery: false
license: ""
---
Recently I need in my day-to-day access to server that are behind a Bastion Host.
<!--more-->

I need to do a deep search into my old notes and I want to share with us how we can use ssh portforward and configure to automatize operations day by day.

SSH Command Port Forward

```bash
ssh -L 80:internal.host:80 user@bastion.host
```
