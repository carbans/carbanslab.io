---
title: "Week 01"
subtitle: "The second week of the year"
date: 2025-01-10T20:25:00+02:00
lastmd: 2025-01-10T20:25:00+02:00
draft: true
author: "Carlos Latorre"

tags: ['100DaysToOffload', 'weeklyupdate', 'weeknote']

toc: false
---

- 🏠 I returned home after spending 17 days away, visiting family and friends.

-  
