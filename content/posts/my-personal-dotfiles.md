---
title: "Dotfiles"
subtitle: ""
date: 2025-02-25T18:30:00+02:00
lastmd: 2025-02-25T18:30:00+02:00
draft: false
author: "Carlos Latorre"

tags: ['100DaysToOffload', 'manage', 'dotfiles']

toc: false
---


Today I want to talk about my dotfiles setup. It's difficult to find a way to store and update your dotfiles in a comfortable way.
I want to speak about my system that I've been using for the last few years, and I love it.

Recently, I talked with a colleague about how to configure a new WSL at work. I discussed with him that it only takes me one hour to have everything up and running.
I want to share with you the list of things that I do with a new PC.

## ASDF

This tool is amazing, I discovered this approximately one year ago and I love it. If you read its webpage, it says: "The multiple runtime version manager"
It means that you only need one tool to install every tool that you need to work with your PC. It's extensible, which means you have plugins to install everything you need.
You have the ability to have different executables by path, you can have two versions of Terraform in different folders, and you have a text file that indicates to ASDF which version to use.
You can have this file in the repository. Here are the steps that you need to install and setup the correct versions:

- Add plugin into asdf
```bash
cat .tool-versions | awk '{print $1}' | xargs -I _ asdf plugin add _
```
- Install all tool that you have into .tool-versions file and their versions
```bash
asdf install
```
With these simple two commands I have all my tools installed.

## Chezmoi

With chezmoi you can have your dotfiles and configurations saved into a Git repository and you can take them with you. Installing chezmoi is easy because you have a plugin for asdf :P.
After that you only need to initialize chezmoi with a simple command:

```bash
chezmoi init --apply $GITHUB_USER
```

And the magic 🪄 is done. Every configuration  you had in the  previous environment  is now in this environment. 

## Gopass

If you have secrets you need to carry with you, the best way that I've found to do it is by adding them into Gopass. Gopass is a tool that uses GPG keys and Git repositories to set up a secure vault for secrets.
```bash
asdf plugin add gopass
```
Yes it's easy! and you need to import your private gpg key, and after that you can use a Chezmoi template with a gopass string like:
```
{{ gopass "$PASS_NAME" }}
```

You have the link below at my github repository with my dotfiles.

Links:
 - https://asdf-vm.com/
 - https://www.chezmoi.io/
 - https://www.gopass.pw/
 - https://github.com/carbans/dotfiles

> Post 1/100 - Round 1 of the [100DaysToOffload](https://100daystooffload.com/) challenge!

