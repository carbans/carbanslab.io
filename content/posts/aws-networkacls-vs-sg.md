---
title: "Network ACL vs Security Groups AWS"
subtitle: ""
date: 2024-02-12T19:30:58+02:00
lastmod: 2024-02-12T19:30:58+02:00
draft: false
author: "Carlos Latorre"
authorLink: ""
description: ""

tags: ['aws', 'networking', 'sg', 'nacl']
categories: ['networking', 'cloud']

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: ""
featuredImagePreview: ""

toc: false
math:
  enable: false
lightgallery: false
license: ""
---
Hi net!

In my day-to-day, AWS represents over 90% of the time I spend working. That's why there are concepts that I have deeply internalized but I believe many people are unaware of or not entirely clear on, especially those who don't use the cloud on a daily basis.
AWS cloud infrastructure, security is paramount to safeguarding users' resources and data. Two primary tools for managing security in AWS are Security Groups and Network ACLs (Access Control Lists). While both serve a similar function in controlling inbound and outbound traffic, there are fundamental differences in their operation and applications. In this article, we will explore the key differences between Security Groups and Network ACLs.

Now, let's delve into a detailed table to analysis of each of these AWS cloud security components.

| Feature                        | Security Groups       | Network ACLs          |
|---------------------------------|-----------------------|-----------------------|
| **Location**                   | Associated with ENIs | Associated with subnets |
| **OSI Layer Level**            | Layer 4 (Stateful firewall) | Layer 3 (Rule-based firewall) |
| **Control Rules**              | Based on allow rules  | Based on allow or deny rules |
| **Connection State**           | Controls both inbound and outbound traffic | Controls both inbound and outbound traffic |
| **Implicit Rules**             | None                  | Implicit rules allow all traffic |
| **Rule Priority**              | Unspecified priority  | Rules are evaluated sequentially |
| **Number of Rules**            | Up to 60 rules per group | Up to 20 rules per list |
| **Rules by IP Address**        | Yes (can filter by IP address) | Yes (can filter by IP address) |
| **Rules by Protocol and Port** | Yes (can specify protocols and ports) | Yes (can specify protocols and ports) |
| **Network Impact**             | More secure as rules can be specific to individual instances | Less secure as they apply to all instances in the subnet |
| **Common Use Cases**           | Specific applications, microsegmentation | Filtering traffic across the entire subnet |


**Security Groups:** Security Groups are security rules associated with individual instances in a network. They operate at Layer 4 (transport layer) and are stateful firewalls, meaning they keep track of connection state. They can allow or deny traffic based on specific rules, such as protocol, port, and IP address. Security Groups provide a high level of security and allow for precise microsegmentation, making them ideal for specific applications.

**Network ACLs:** Network ACLs are security rules applied at Layer 3 (network layer) and are associated with subnets rather than individual instances. They operate based on allow or deny rules and are applied sequentially. Network ACLs are less secure than Security Groups because they affect all instances in a subnet. They are useful for filtering traffic across the entire subnet but do not allow for as precise segmentation as Security Groups.

In summary, the primary difference lies in the network layer they operate at, the granularity of rules, and the location to which they are applied. Security Groups are ideal when a high level of security and instance-level control is needed, while Network ACLs are useful for applying rules at the subnet level and filtering traffic on that scale.

I hope this article has provided clarity and differentiated the usage of these two crucial components in AWS cloud security.  It's important to note that while Security Groups and Network ACLs have their distinct roles, they are complementary in creating comprehensive security strategies within the AWS environment.

For audit or consultancy needs, feel free to reach out to me for assistance with your infrastructures.


**Happy Hacking**
**Bye!**
